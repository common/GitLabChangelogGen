# Script for generating Changelog based on GitLabs Merge Requests

## Usage

```bash
cd CLONE_PATH
git clone http://gitlab.gmrv.es/common/GitLabChangelogGen
cd EXISTING_REPO
CLONE_PATH/gitlabChangelogGen.sh
```
