#!/bin/bash

echo "# "$(grep project CMakeLists.txt  | head -n1 | cut -d "(" -f2 | sed "s/^[ \t]*//"  | cut -d " " -f1)
echo ""
echo "## git master"
echo ""


#Dump output of "git log" command to a string
LOG=$(git log 2>&1)


tagCommitLogLine=""
tagCommits=""
tags=""
for tag in `git tag -l`; do
    tagCommit=`git show $tag | grep commit  | cut -d" " -f2`
    tagCommitLogLine=$(echo "${LOG}" | grep $tagCommit -n | cut -d: -f1);
    if [ ! -z $tagCommitLogLine ] && [ ! -z $tagCommit ] && [ ! -z $tag ]; then
        tagCommitLogLines=$tagCommitLogLine" "$tagCommitLogLines
        tagCommits=$tagCommit' '$tagCommits
        tags=$tag' '$tags
    fi
done

tagCommitsCombined=$(paste -d' ' <(echo "$tagCommitLogLines" | sed -e "s/ /\n/g") <(echo "$tagCommits" | sed -e "s/ /\n/g") <(echo "$tags" | sed -e "s/ /\n/g"))

$(paste -d' ' <(echo "$tagCommitLogLines" | sed -e "s/ /\n/g") <(echo "$tagCommits" | sed -e "s/ /\n/g") <(echo "$tags" | sed -e "s/ /\n/g") | sort -n | tail -n +2 > .tagscombined)

tagCommit=$(cat .tagscombined | head -n1)
cp .tagscombined .tagscombined.old
tail -n +2 .tagscombined.old > .tagscombined

#Search for "See merge request !xxx" lines
for mrLine in $(echo "${LOG}" | grep "See merge request" -n | cut -d: -f1);
do

    if [ ! -z "$tagCommit" ]; then
        if [ $(echo $tagCommit | cut -d" " -f1) -le $mrLine ]; then
            echo
            echo "## Release "$(echo $tagCommit | cut -d" " -f3)
            echo
            tagCommit=$(cat .tagscombined | head -n1)
            cp .tagscombined .tagscombined.old
            tail -n +2 .tagscombined.old > .tagscombined
        fi
    fi

    #Guess the start and end lines of this block
    let startLine=mrLine-3
    let endLine=mrLine-2

    while [ 1 ]
    do
        #Check if startLine is like "Merge branch xxxx into yyyy"
        echo "${LOG}" | sed "${startLine}q;d" | grep -q -e "Merge branch" -e "into"
        if [[ $? -eq 0 ]];
        then
            #Cut from the next line
            let startLine=${startLine}+1
            break
        else
            #Continue search backward
            let startLine=${startLine}-1
        fi
    done

    #Cut LOG lines from ${startLine} to ${endLine}
    mrMsg=$(echo "$LOG" | sed -n "${startLine},${endLine}p")

    #Get the referenced merge-request number in line mrLine
    mrNum=$(echo "${LOG}" | sed "${mrLine}q;d" | cut -d! -f2)

    #Dump output
    echo "* [!"${mrNum}"]" $(echo ${mrMsg} | cut -d"@" -f1)

done
